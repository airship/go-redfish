module opendev.org/airship/go-redfish

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/stretchr/testify v1.7.0
	opendev.org/airship/go-redfish/client v0.0.0-20220825191957-470a7752c62d
)
