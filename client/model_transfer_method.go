/*
 * Redfish OAPI specification
 *
 * Partial Redfish OAPI specification for a limited client
 *
 * API version: 0.0.1
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package client

import (
	"encoding/json"
	"fmt"
)

// TransferMethod the model 'TransferMethod'
type TransferMethod string

// List of TransferMethod
const (
	TRANSFERMETHOD_STREAM TransferMethod = "Stream"
	TRANSFERMETHOD_UPLOAD TransferMethod = "Upload"
)

func (v *TransferMethod) UnmarshalJSON(src []byte) error {
	var value string
	err := json.Unmarshal(src, &value)
	if err != nil {
		return err
	}
	enumTypeValue := TransferMethod(value)
	for _, existing := range []TransferMethod{ "Stream", "Upload",   } {
		if existing == enumTypeValue {
			*v = enumTypeValue
			return nil
		}
	}

	return fmt.Errorf("%+v is not a valid TransferMethod", value)
}

// Ptr returns reference to TransferMethod value
func (v TransferMethod) Ptr() *TransferMethod {
	return &v
}

type NullableTransferMethod struct {
	value *TransferMethod
	isSet bool
}

func (v NullableTransferMethod) Get() *TransferMethod {
	return v.value
}

func (v *NullableTransferMethod) Set(val *TransferMethod) {
	v.value = val
	v.isSet = true
}

func (v NullableTransferMethod) IsSet() bool {
	return v.isSet
}

func (v *NullableTransferMethod) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableTransferMethod(val *TransferMethod) *NullableTransferMethod {
	return &NullableTransferMethod{value: val, isSet: true}
}

func (v NullableTransferMethod) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableTransferMethod) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}

