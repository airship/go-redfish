/*
 * Redfish OAPI specification
 *
 * Partial Redfish OAPI specification for a limited client
 *
 * API version: 0.0.1
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package client

import (
	"encoding/json"
	"fmt"
)

// ConnectedVia the model 'ConnectedVia'
type ConnectedVia string

// List of ConnectedVia
const (
	CONNECTEDVIA_NOT_CONNECTED ConnectedVia = "NotConnected"
	CONNECTEDVIA_URI ConnectedVia = "URI"
	CONNECTEDVIA_APPLET ConnectedVia = "Applet"
	CONNECTEDVIA_OEM ConnectedVia = "Oem"
)

func (v *ConnectedVia) UnmarshalJSON(src []byte) error {
	var value string
	err := json.Unmarshal(src, &value)
	if err != nil {
		return err
	}
	enumTypeValue := ConnectedVia(value)
	for _, existing := range []ConnectedVia{ "NotConnected", "URI", "Applet", "Oem",   } {
		if existing == enumTypeValue {
			*v = enumTypeValue
			return nil
		}
	}

	return fmt.Errorf("%+v is not a valid ConnectedVia", value)
}

// Ptr returns reference to ConnectedVia value
func (v ConnectedVia) Ptr() *ConnectedVia {
	return &v
}

type NullableConnectedVia struct {
	value *ConnectedVia
	isSet bool
}

func (v NullableConnectedVia) Get() *ConnectedVia {
	return v.value
}

func (v *NullableConnectedVia) Set(val *ConnectedVia) {
	v.value = val
	v.isSet = true
}

func (v NullableConnectedVia) IsSet() bool {
	return v.isSet
}

func (v *NullableConnectedVia) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableConnectedVia(val *ConnectedVia) *NullableConnectedVia {
	return &NullableConnectedVia{value: val, isSet: true}
}

func (v NullableConnectedVia) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableConnectedVia) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}

