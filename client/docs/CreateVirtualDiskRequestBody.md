# CreateVirtualDiskRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VolumeType** | [**VolumeType**](VolumeType.md) |  | 
**Name** | **string** |  | 
**Drives** | [**[]IdRef**](IdRef.md) |  | 

## Methods

### NewCreateVirtualDiskRequestBody

`func NewCreateVirtualDiskRequestBody(volumeType VolumeType, name string, drives []IdRef, ) *CreateVirtualDiskRequestBody`

NewCreateVirtualDiskRequestBody instantiates a new CreateVirtualDiskRequestBody object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateVirtualDiskRequestBodyWithDefaults

`func NewCreateVirtualDiskRequestBodyWithDefaults() *CreateVirtualDiskRequestBody`

NewCreateVirtualDiskRequestBodyWithDefaults instantiates a new CreateVirtualDiskRequestBody object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVolumeType

`func (o *CreateVirtualDiskRequestBody) GetVolumeType() VolumeType`

GetVolumeType returns the VolumeType field if non-nil, zero value otherwise.

### GetVolumeTypeOk

`func (o *CreateVirtualDiskRequestBody) GetVolumeTypeOk() (*VolumeType, bool)`

GetVolumeTypeOk returns a tuple with the VolumeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVolumeType

`func (o *CreateVirtualDiskRequestBody) SetVolumeType(v VolumeType)`

SetVolumeType sets VolumeType field to given value.


### GetName

`func (o *CreateVirtualDiskRequestBody) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CreateVirtualDiskRequestBody) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CreateVirtualDiskRequestBody) SetName(v string)`

SetName sets Name field to given value.


### GetDrives

`func (o *CreateVirtualDiskRequestBody) GetDrives() []IdRef`

GetDrives returns the Drives field if non-nil, zero value otherwise.

### GetDrivesOk

`func (o *CreateVirtualDiskRequestBody) GetDrivesOk() (*[]IdRef, bool)`

GetDrivesOk returns a tuple with the Drives field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDrives

`func (o *CreateVirtualDiskRequestBody) SetDrives(v []IdRef)`

SetDrives sets Drives field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


