# SoftwareInventory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OdataType** | **string** |  | [readonly] 
**OdataId** | **string** |  | [readonly] 
**OdataContext** | Pointer to **string** |  | [optional] [readonly] 
**OdataEtag** | Pointer to **string** |  | [optional] [readonly] 
**Description** | Pointer to **NullableString** |  | [optional] [readonly] 
**Id** | **string** |  | [readonly] 
**LowestSupportedVersion** | Pointer to **NullableString** |  | [optional] [readonly] 
**Manufacturer** | Pointer to **NullableString** |  | [optional] [readonly] 
**Name** | **string** |  | [readonly] 
**RelatedItem** | Pointer to [**[]IdRef**](IdRef.md) |  | [optional] [readonly] 
**RelatedItemodataCount** | Pointer to **NullableInt32** |  | [optional] [readonly] 
**ReleaseDate** | Pointer to **NullableTime** |  | [optional] [readonly] 
**SoftwareId** | Pointer to **string** |  | [optional] [readonly] 
**Status** | Pointer to [**Status**](Status.md) |  | [optional] 
**UefiDevicePaths** | Pointer to **[]string** |  | [optional] [readonly] 
**Updateable** | **NullableBool** |  | [readonly] 
**Version** | **NullableString** |  | [readonly] 

## Methods

### NewSoftwareInventory

`func NewSoftwareInventory(odataType string, odataId string, id string, name string, updateable NullableBool, version NullableString, ) *SoftwareInventory`

NewSoftwareInventory instantiates a new SoftwareInventory object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSoftwareInventoryWithDefaults

`func NewSoftwareInventoryWithDefaults() *SoftwareInventory`

NewSoftwareInventoryWithDefaults instantiates a new SoftwareInventory object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOdataType

`func (o *SoftwareInventory) GetOdataType() string`

GetOdataType returns the OdataType field if non-nil, zero value otherwise.

### GetOdataTypeOk

`func (o *SoftwareInventory) GetOdataTypeOk() (*string, bool)`

GetOdataTypeOk returns a tuple with the OdataType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOdataType

`func (o *SoftwareInventory) SetOdataType(v string)`

SetOdataType sets OdataType field to given value.


### GetOdataId

`func (o *SoftwareInventory) GetOdataId() string`

GetOdataId returns the OdataId field if non-nil, zero value otherwise.

### GetOdataIdOk

`func (o *SoftwareInventory) GetOdataIdOk() (*string, bool)`

GetOdataIdOk returns a tuple with the OdataId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOdataId

`func (o *SoftwareInventory) SetOdataId(v string)`

SetOdataId sets OdataId field to given value.


### GetOdataContext

`func (o *SoftwareInventory) GetOdataContext() string`

GetOdataContext returns the OdataContext field if non-nil, zero value otherwise.

### GetOdataContextOk

`func (o *SoftwareInventory) GetOdataContextOk() (*string, bool)`

GetOdataContextOk returns a tuple with the OdataContext field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOdataContext

`func (o *SoftwareInventory) SetOdataContext(v string)`

SetOdataContext sets OdataContext field to given value.

### HasOdataContext

`func (o *SoftwareInventory) HasOdataContext() bool`

HasOdataContext returns a boolean if a field has been set.

### GetOdataEtag

`func (o *SoftwareInventory) GetOdataEtag() string`

GetOdataEtag returns the OdataEtag field if non-nil, zero value otherwise.

### GetOdataEtagOk

`func (o *SoftwareInventory) GetOdataEtagOk() (*string, bool)`

GetOdataEtagOk returns a tuple with the OdataEtag field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOdataEtag

`func (o *SoftwareInventory) SetOdataEtag(v string)`

SetOdataEtag sets OdataEtag field to given value.

### HasOdataEtag

`func (o *SoftwareInventory) HasOdataEtag() bool`

HasOdataEtag returns a boolean if a field has been set.

### GetDescription

`func (o *SoftwareInventory) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *SoftwareInventory) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *SoftwareInventory) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *SoftwareInventory) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### SetDescriptionNil

`func (o *SoftwareInventory) SetDescriptionNil(b bool)`

 SetDescriptionNil sets the value for Description to be an explicit nil

### UnsetDescription
`func (o *SoftwareInventory) UnsetDescription()`

UnsetDescription ensures that no value is present for Description, not even an explicit nil
### GetId

`func (o *SoftwareInventory) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *SoftwareInventory) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *SoftwareInventory) SetId(v string)`

SetId sets Id field to given value.


### GetLowestSupportedVersion

`func (o *SoftwareInventory) GetLowestSupportedVersion() string`

GetLowestSupportedVersion returns the LowestSupportedVersion field if non-nil, zero value otherwise.

### GetLowestSupportedVersionOk

`func (o *SoftwareInventory) GetLowestSupportedVersionOk() (*string, bool)`

GetLowestSupportedVersionOk returns a tuple with the LowestSupportedVersion field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLowestSupportedVersion

`func (o *SoftwareInventory) SetLowestSupportedVersion(v string)`

SetLowestSupportedVersion sets LowestSupportedVersion field to given value.

### HasLowestSupportedVersion

`func (o *SoftwareInventory) HasLowestSupportedVersion() bool`

HasLowestSupportedVersion returns a boolean if a field has been set.

### SetLowestSupportedVersionNil

`func (o *SoftwareInventory) SetLowestSupportedVersionNil(b bool)`

 SetLowestSupportedVersionNil sets the value for LowestSupportedVersion to be an explicit nil

### UnsetLowestSupportedVersion
`func (o *SoftwareInventory) UnsetLowestSupportedVersion()`

UnsetLowestSupportedVersion ensures that no value is present for LowestSupportedVersion, not even an explicit nil
### GetManufacturer

`func (o *SoftwareInventory) GetManufacturer() string`

GetManufacturer returns the Manufacturer field if non-nil, zero value otherwise.

### GetManufacturerOk

`func (o *SoftwareInventory) GetManufacturerOk() (*string, bool)`

GetManufacturerOk returns a tuple with the Manufacturer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetManufacturer

`func (o *SoftwareInventory) SetManufacturer(v string)`

SetManufacturer sets Manufacturer field to given value.

### HasManufacturer

`func (o *SoftwareInventory) HasManufacturer() bool`

HasManufacturer returns a boolean if a field has been set.

### SetManufacturerNil

`func (o *SoftwareInventory) SetManufacturerNil(b bool)`

 SetManufacturerNil sets the value for Manufacturer to be an explicit nil

### UnsetManufacturer
`func (o *SoftwareInventory) UnsetManufacturer()`

UnsetManufacturer ensures that no value is present for Manufacturer, not even an explicit nil
### GetName

`func (o *SoftwareInventory) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *SoftwareInventory) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *SoftwareInventory) SetName(v string)`

SetName sets Name field to given value.


### GetRelatedItem

`func (o *SoftwareInventory) GetRelatedItem() []IdRef`

GetRelatedItem returns the RelatedItem field if non-nil, zero value otherwise.

### GetRelatedItemOk

`func (o *SoftwareInventory) GetRelatedItemOk() (*[]IdRef, bool)`

GetRelatedItemOk returns a tuple with the RelatedItem field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRelatedItem

`func (o *SoftwareInventory) SetRelatedItem(v []IdRef)`

SetRelatedItem sets RelatedItem field to given value.

### HasRelatedItem

`func (o *SoftwareInventory) HasRelatedItem() bool`

HasRelatedItem returns a boolean if a field has been set.

### GetRelatedItemodataCount

`func (o *SoftwareInventory) GetRelatedItemodataCount() int32`

GetRelatedItemodataCount returns the RelatedItemodataCount field if non-nil, zero value otherwise.

### GetRelatedItemodataCountOk

`func (o *SoftwareInventory) GetRelatedItemodataCountOk() (*int32, bool)`

GetRelatedItemodataCountOk returns a tuple with the RelatedItemodataCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRelatedItemodataCount

`func (o *SoftwareInventory) SetRelatedItemodataCount(v int32)`

SetRelatedItemodataCount sets RelatedItemodataCount field to given value.

### HasRelatedItemodataCount

`func (o *SoftwareInventory) HasRelatedItemodataCount() bool`

HasRelatedItemodataCount returns a boolean if a field has been set.

### SetRelatedItemodataCountNil

`func (o *SoftwareInventory) SetRelatedItemodataCountNil(b bool)`

 SetRelatedItemodataCountNil sets the value for RelatedItemodataCount to be an explicit nil

### UnsetRelatedItemodataCount
`func (o *SoftwareInventory) UnsetRelatedItemodataCount()`

UnsetRelatedItemodataCount ensures that no value is present for RelatedItemodataCount, not even an explicit nil
### GetReleaseDate

`func (o *SoftwareInventory) GetReleaseDate() time.Time`

GetReleaseDate returns the ReleaseDate field if non-nil, zero value otherwise.

### GetReleaseDateOk

`func (o *SoftwareInventory) GetReleaseDateOk() (*time.Time, bool)`

GetReleaseDateOk returns a tuple with the ReleaseDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReleaseDate

`func (o *SoftwareInventory) SetReleaseDate(v time.Time)`

SetReleaseDate sets ReleaseDate field to given value.

### HasReleaseDate

`func (o *SoftwareInventory) HasReleaseDate() bool`

HasReleaseDate returns a boolean if a field has been set.

### SetReleaseDateNil

`func (o *SoftwareInventory) SetReleaseDateNil(b bool)`

 SetReleaseDateNil sets the value for ReleaseDate to be an explicit nil

### UnsetReleaseDate
`func (o *SoftwareInventory) UnsetReleaseDate()`

UnsetReleaseDate ensures that no value is present for ReleaseDate, not even an explicit nil
### GetSoftwareId

`func (o *SoftwareInventory) GetSoftwareId() string`

GetSoftwareId returns the SoftwareId field if non-nil, zero value otherwise.

### GetSoftwareIdOk

`func (o *SoftwareInventory) GetSoftwareIdOk() (*string, bool)`

GetSoftwareIdOk returns a tuple with the SoftwareId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSoftwareId

`func (o *SoftwareInventory) SetSoftwareId(v string)`

SetSoftwareId sets SoftwareId field to given value.

### HasSoftwareId

`func (o *SoftwareInventory) HasSoftwareId() bool`

HasSoftwareId returns a boolean if a field has been set.

### GetStatus

`func (o *SoftwareInventory) GetStatus() Status`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *SoftwareInventory) GetStatusOk() (*Status, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *SoftwareInventory) SetStatus(v Status)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *SoftwareInventory) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetUefiDevicePaths

`func (o *SoftwareInventory) GetUefiDevicePaths() []string`

GetUefiDevicePaths returns the UefiDevicePaths field if non-nil, zero value otherwise.

### GetUefiDevicePathsOk

`func (o *SoftwareInventory) GetUefiDevicePathsOk() (*[]string, bool)`

GetUefiDevicePathsOk returns a tuple with the UefiDevicePaths field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUefiDevicePaths

`func (o *SoftwareInventory) SetUefiDevicePaths(v []string)`

SetUefiDevicePaths sets UefiDevicePaths field to given value.

### HasUefiDevicePaths

`func (o *SoftwareInventory) HasUefiDevicePaths() bool`

HasUefiDevicePaths returns a boolean if a field has been set.

### GetUpdateable

`func (o *SoftwareInventory) GetUpdateable() bool`

GetUpdateable returns the Updateable field if non-nil, zero value otherwise.

### GetUpdateableOk

`func (o *SoftwareInventory) GetUpdateableOk() (*bool, bool)`

GetUpdateableOk returns a tuple with the Updateable field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdateable

`func (o *SoftwareInventory) SetUpdateable(v bool)`

SetUpdateable sets Updateable field to given value.


### SetUpdateableNil

`func (o *SoftwareInventory) SetUpdateableNil(b bool)`

 SetUpdateableNil sets the value for Updateable to be an explicit nil

### UnsetUpdateable
`func (o *SoftwareInventory) UnsetUpdateable()`

UnsetUpdateable ensures that no value is present for Updateable, not even an explicit nil
### GetVersion

`func (o *SoftwareInventory) GetVersion() string`

GetVersion returns the Version field if non-nil, zero value otherwise.

### GetVersionOk

`func (o *SoftwareInventory) GetVersionOk() (*string, bool)`

GetVersionOk returns a tuple with the Version field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersion

`func (o *SoftwareInventory) SetVersion(v string)`

SetVersion sets Version field to given value.


### SetVersionNil

`func (o *SoftwareInventory) SetVersionNil(b bool)`

 SetVersionNil sets the value for Version to be an explicit nil

### UnsetVersion
`func (o *SoftwareInventory) UnsetVersion()`

UnsetVersion ensures that no value is present for Version, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


