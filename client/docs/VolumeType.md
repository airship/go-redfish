# VolumeType

## Enum


* `RAW_DEVICE` (value: `"RawDevice"`)

* `NON_REDUNDANT` (value: `"NonRedundant"`)

* `MIRRORED` (value: `"Mirrored"`)

* `STRIPED_WITH_PARITY` (value: `"StripedWithParity"`)

* `SPANNED_MIRRORS` (value: `"SpannedMirrors"`)

* `SPANNED_STRIPES_WITH_PARITY` (value: `"SpannedStripesWithParity"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


