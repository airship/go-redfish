# InlineObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SoftwareImage** | Pointer to ***os.File** |  | [optional] 

## Methods

### NewInlineObject

`func NewInlineObject() *InlineObject`

NewInlineObject instantiates a new InlineObject object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineObjectWithDefaults

`func NewInlineObjectWithDefaults() *InlineObject`

NewInlineObjectWithDefaults instantiates a new InlineObject object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSoftwareImage

`func (o *InlineObject) GetSoftwareImage() *os.File`

GetSoftwareImage returns the SoftwareImage field if non-nil, zero value otherwise.

### GetSoftwareImageOk

`func (o *InlineObject) GetSoftwareImageOk() (**os.File, bool)`

GetSoftwareImageOk returns a tuple with the SoftwareImage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSoftwareImage

`func (o *InlineObject) SetSoftwareImage(v *os.File)`

SetSoftwareImage sets SoftwareImage field to given value.

### HasSoftwareImage

`func (o *InlineObject) HasSoftwareImage() bool`

HasSoftwareImage returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


