# IDRACResetRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ResetType** | Pointer to [**IDRACResetType**](IDRACResetType.md) |  | [optional] 

## Methods

### NewIDRACResetRequestBody

`func NewIDRACResetRequestBody() *IDRACResetRequestBody`

NewIDRACResetRequestBody instantiates a new IDRACResetRequestBody object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIDRACResetRequestBodyWithDefaults

`func NewIDRACResetRequestBodyWithDefaults() *IDRACResetRequestBody`

NewIDRACResetRequestBodyWithDefaults instantiates a new IDRACResetRequestBody object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResetType

`func (o *IDRACResetRequestBody) GetResetType() IDRACResetType`

GetResetType returns the ResetType field if non-nil, zero value otherwise.

### GetResetTypeOk

`func (o *IDRACResetRequestBody) GetResetTypeOk() (*IDRACResetType, bool)`

GetResetTypeOk returns a tuple with the ResetType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResetType

`func (o *IDRACResetRequestBody) SetResetType(v IDRACResetType)`

SetResetType sets ResetType field to given value.

### HasResetType

`func (o *IDRACResetRequestBody) HasResetType() bool`

HasResetType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


