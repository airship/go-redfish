# \DefaultApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateVirtualDisk**](DefaultApi.md#CreateVirtualDisk) | **Post** /redfish/v1/Systems/{ComputerSystemId}/Storage/{StorageControllerId}/Volumes | 
[**DeleteVirtualdisk**](DefaultApi.md#DeleteVirtualdisk) | **Delete** /redfish/v1/Systems/{ComputerSystemId}/Storage/Volumes/{StorageId} | 
[**EjectVirtualMedia**](DefaultApi.md#EjectVirtualMedia) | **Post** /redfish/v1/Managers/{managerId}/VirtualMedia/{virtualMediaId}/Actions/VirtualMedia.EjectMedia | 
[**FirmwareInventory**](DefaultApi.md#FirmwareInventory) | **Get** /redfish/v1/UpdateService/FirmwareInventory | 
[**FirmwareInventoryDownloadImage**](DefaultApi.md#FirmwareInventoryDownloadImage) | **Post** /redfish/v1/UpdateService/FirmwareInventory | 
[**GetManager**](DefaultApi.md#GetManager) | **Get** /redfish/v1/Managers/{managerId} | 
[**GetManagerVirtualMedia**](DefaultApi.md#GetManagerVirtualMedia) | **Get** /redfish/v1/Managers/{managerId}/VirtualMedia/{virtualMediaId} | 
[**GetRoot**](DefaultApi.md#GetRoot) | **Get** /redfish/v1 | 
[**GetSoftwareInventory**](DefaultApi.md#GetSoftwareInventory) | **Get** /redfish/v1/UpdateService/FirmwareInventory/{softwareId} | 
[**GetSystem**](DefaultApi.md#GetSystem) | **Get** /redfish/v1/Systems/{systemId} | 
[**GetTask**](DefaultApi.md#GetTask) | **Get** /redfish/v1/TaskService/Tasks/{taskId} | 
[**GetTaskList**](DefaultApi.md#GetTaskList) | **Get** /redfish/v1/TaskService/Tasks | 
[**GetVolumes**](DefaultApi.md#GetVolumes) | **Get** /redfish/v1/Systems/{ComputerSystemId}/Storage/{StorageControllerId}/Volumes | 
[**InsertVirtualMedia**](DefaultApi.md#InsertVirtualMedia) | **Post** /redfish/v1/Managers/{managerId}/VirtualMedia/{virtualMediaId}/Actions/VirtualMedia.InsertMedia | 
[**ListManagerVirtualMedia**](DefaultApi.md#ListManagerVirtualMedia) | **Get** /redfish/v1/Managers/{managerId}/VirtualMedia | 
[**ListManagers**](DefaultApi.md#ListManagers) | **Get** /redfish/v1/Managers | 
[**ListSystems**](DefaultApi.md#ListSystems) | **Get** /redfish/v1/Systems | 
[**ResetIdrac**](DefaultApi.md#ResetIdrac) | **Post** /redfish/v1/Managers/iDRAC.Embedded.1/Actions/Manager.Reset | 
[**ResetSystem**](DefaultApi.md#ResetSystem) | **Post** /redfish/v1/Systems/{ComputerSystemId}/Actions/ComputerSystem.Reset | 
[**SetSystem**](DefaultApi.md#SetSystem) | **Patch** /redfish/v1/Systems/{systemId} | 
[**UpdateService**](DefaultApi.md#UpdateService) | **Get** /redfish/v1/UpdateService | 
[**UpdateServiceSimpleUpdate**](DefaultApi.md#UpdateServiceSimpleUpdate) | **Post** /redfish/v1/UpdateService/Actions/UpdateService.SimpleUpdate | 



## CreateVirtualDisk

> RedfishError CreateVirtualDisk(ctx, computerSystemId, storageControllerId).CreateVirtualDiskRequestBody(createVirtualDiskRequestBody).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    computerSystemId := "computerSystemId_example" // string | System ID
    storageControllerId := "storageControllerId_example" // string | Storage Controller ID
    createVirtualDiskRequestBody := *openapiclient.NewCreateVirtualDiskRequestBody(openapiclient.VolumeType("RawDevice"), "Name_example", []openapiclient.IdRef{*openapiclient.NewIdRef()}) // CreateVirtualDiskRequestBody | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.CreateVirtualDisk(context.Background(), computerSystemId, storageControllerId).CreateVirtualDiskRequestBody(createVirtualDiskRequestBody).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.CreateVirtualDisk``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateVirtualDisk`: RedfishError
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.CreateVirtualDisk`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**computerSystemId** | **string** | System ID | 
**storageControllerId** | **string** | Storage Controller ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateVirtualDiskRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **createVirtualDiskRequestBody** | [**CreateVirtualDiskRequestBody**](CreateVirtualDiskRequestBody.md) |  | 

### Return type

[**RedfishError**](RedfishError.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteVirtualdisk

> DeleteVirtualdisk(ctx, computerSystemId, storageId).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    computerSystemId := "computerSystemId_example" // string | System ID
    storageId := "storageId_example" // string | Storage ID

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.DeleteVirtualdisk(context.Background(), computerSystemId, storageId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.DeleteVirtualdisk``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**computerSystemId** | **string** | System ID | 
**storageId** | **string** | Storage ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteVirtualdiskRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## EjectVirtualMedia

> RedfishError EjectVirtualMedia(ctx, managerId, virtualMediaId).Body(body).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    managerId := "managerId_example" // string | ID of resource
    virtualMediaId := "virtualMediaId_example" // string | ID of resource
    body := map[string]interface{}(Object) // map[string]interface{} | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.EjectVirtualMedia(context.Background(), managerId, virtualMediaId).Body(body).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.EjectVirtualMedia``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `EjectVirtualMedia`: RedfishError
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.EjectVirtualMedia`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**managerId** | **string** | ID of resource | 
**virtualMediaId** | **string** | ID of resource | 

### Other Parameters

Other parameters are passed through a pointer to a apiEjectVirtualMediaRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **body** | **map[string]interface{}** |  | 

### Return type

[**RedfishError**](RedfishError.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## FirmwareInventory

> Collection FirmwareInventory(ctx).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.FirmwareInventory(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.FirmwareInventory``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `FirmwareInventory`: Collection
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.FirmwareInventory`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiFirmwareInventoryRequest struct via the builder pattern


### Return type

[**Collection**](Collection.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## FirmwareInventoryDownloadImage

> RedfishError FirmwareInventoryDownloadImage(ctx).SoftwareImage(softwareImage).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    softwareImage := os.NewFile(1234, "some_file") // *os.File |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.FirmwareInventoryDownloadImage(context.Background()).SoftwareImage(softwareImage).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.FirmwareInventoryDownloadImage``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `FirmwareInventoryDownloadImage`: RedfishError
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.FirmwareInventoryDownloadImage`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiFirmwareInventoryDownloadImageRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **softwareImage** | ***os.File** |  | 

### Return type

[**RedfishError**](RedfishError.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/formdata
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetManager

> Manager GetManager(ctx, managerId).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    managerId := "managerId_example" // string | ID of resource

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.GetManager(context.Background(), managerId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.GetManager``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetManager`: Manager
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.GetManager`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**managerId** | **string** | ID of resource | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetManagerRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Manager**](Manager.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetManagerVirtualMedia

> VirtualMedia GetManagerVirtualMedia(ctx, managerId, virtualMediaId).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    managerId := "managerId_example" // string | ID of resource
    virtualMediaId := "virtualMediaId_example" // string | ID of resource

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.GetManagerVirtualMedia(context.Background(), managerId, virtualMediaId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.GetManagerVirtualMedia``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetManagerVirtualMedia`: VirtualMedia
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.GetManagerVirtualMedia`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**managerId** | **string** | ID of resource | 
**virtualMediaId** | **string** | ID of resource | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetManagerVirtualMediaRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**VirtualMedia**](VirtualMedia.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetRoot

> Root GetRoot(ctx).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.GetRoot(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.GetRoot``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetRoot`: Root
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.GetRoot`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetRootRequest struct via the builder pattern


### Return type

[**Root**](Root.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetSoftwareInventory

> SoftwareInventory GetSoftwareInventory(ctx, softwareId).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    softwareId := "softwareId_example" // string | ID of resource

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.GetSoftwareInventory(context.Background(), softwareId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.GetSoftwareInventory``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetSoftwareInventory`: SoftwareInventory
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.GetSoftwareInventory`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**softwareId** | **string** | ID of resource | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetSoftwareInventoryRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**SoftwareInventory**](SoftwareInventory.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetSystem

> ComputerSystem GetSystem(ctx, systemId).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    systemId := "systemId_example" // string | ID of resource

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.GetSystem(context.Background(), systemId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.GetSystem``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetSystem`: ComputerSystem
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.GetSystem`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**systemId** | **string** | ID of resource | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetSystemRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**ComputerSystem**](ComputerSystem.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTask

> Task GetTask(ctx, taskId).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    taskId := "taskId_example" // string | Task ID

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.GetTask(context.Background(), taskId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.GetTask``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTask`: Task
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.GetTask`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**taskId** | **string** | Task ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetTaskRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Task**](Task.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTaskList

> Collection GetTaskList(ctx).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.GetTaskList(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.GetTaskList``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTaskList`: Collection
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.GetTaskList`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetTaskListRequest struct via the builder pattern


### Return type

[**Collection**](Collection.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVolumes

> Collection GetVolumes(ctx, computerSystemId, storageControllerId).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    computerSystemId := "computerSystemId_example" // string | System ID
    storageControllerId := "storageControllerId_example" // string | Storage Controller ID

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.GetVolumes(context.Background(), computerSystemId, storageControllerId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.GetVolumes``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVolumes`: Collection
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.GetVolumes`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**computerSystemId** | **string** | System ID | 
**storageControllerId** | **string** | Storage Controller ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetVolumesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Collection**](Collection.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## InsertVirtualMedia

> RedfishError InsertVirtualMedia(ctx, managerId, virtualMediaId).InsertMediaRequestBody(insertMediaRequestBody).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    managerId := "managerId_example" // string | ID of resource
    virtualMediaId := "virtualMediaId_example" // string | ID of resource
    insertMediaRequestBody := *openapiclient.NewInsertMediaRequestBody("Image_example") // InsertMediaRequestBody | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.InsertVirtualMedia(context.Background(), managerId, virtualMediaId).InsertMediaRequestBody(insertMediaRequestBody).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.InsertVirtualMedia``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `InsertVirtualMedia`: RedfishError
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.InsertVirtualMedia`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**managerId** | **string** | ID of resource | 
**virtualMediaId** | **string** | ID of resource | 

### Other Parameters

Other parameters are passed through a pointer to a apiInsertVirtualMediaRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **insertMediaRequestBody** | [**InsertMediaRequestBody**](InsertMediaRequestBody.md) |  | 

### Return type

[**RedfishError**](RedfishError.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListManagerVirtualMedia

> Collection ListManagerVirtualMedia(ctx, managerId).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    managerId := "managerId_example" // string | ID of resource

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.ListManagerVirtualMedia(context.Background(), managerId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.ListManagerVirtualMedia``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListManagerVirtualMedia`: Collection
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.ListManagerVirtualMedia`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**managerId** | **string** | ID of resource | 

### Other Parameters

Other parameters are passed through a pointer to a apiListManagerVirtualMediaRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Collection**](Collection.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListManagers

> Collection ListManagers(ctx).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.ListManagers(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.ListManagers``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListManagers`: Collection
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.ListManagers`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiListManagersRequest struct via the builder pattern


### Return type

[**Collection**](Collection.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListSystems

> Collection ListSystems(ctx).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.ListSystems(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.ListSystems``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListSystems`: Collection
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.ListSystems`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiListSystemsRequest struct via the builder pattern


### Return type

[**Collection**](Collection.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ResetIdrac

> ResetIdrac(ctx).IDRACResetRequestBody(iDRACResetRequestBody).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    iDRACResetRequestBody := *openapiclient.NewIDRACResetRequestBody() // IDRACResetRequestBody | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.ResetIdrac(context.Background()).IDRACResetRequestBody(iDRACResetRequestBody).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.ResetIdrac``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiResetIdracRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iDRACResetRequestBody** | [**IDRACResetRequestBody**](IDRACResetRequestBody.md) |  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ResetSystem

> RedfishError ResetSystem(ctx, computerSystemId).ResetRequestBody(resetRequestBody).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    computerSystemId := "computerSystemId_example" // string | 
    resetRequestBody := *openapiclient.NewResetRequestBody() // ResetRequestBody | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.ResetSystem(context.Background(), computerSystemId).ResetRequestBody(resetRequestBody).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.ResetSystem``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ResetSystem`: RedfishError
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.ResetSystem`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**computerSystemId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiResetSystemRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **resetRequestBody** | [**ResetRequestBody**](ResetRequestBody.md) |  | 

### Return type

[**RedfishError**](RedfishError.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetSystem

> ComputerSystem SetSystem(ctx, systemId).ComputerSystem(computerSystem).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    systemId := "systemId_example" // string | ID of resource
    computerSystem := *openapiclient.NewComputerSystem() // ComputerSystem | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.SetSystem(context.Background(), systemId).ComputerSystem(computerSystem).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.SetSystem``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SetSystem`: ComputerSystem
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.SetSystem`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**systemId** | **string** | ID of resource | 

### Other Parameters

Other parameters are passed through a pointer to a apiSetSystemRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **computerSystem** | [**ComputerSystem**](ComputerSystem.md) |  | 

### Return type

[**ComputerSystem**](ComputerSystem.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateService

> UpdateService UpdateService(ctx).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.UpdateService(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.UpdateService``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateService`: UpdateService
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.UpdateService`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateServiceRequest struct via the builder pattern


### Return type

[**UpdateService**](UpdateService.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateServiceSimpleUpdate

> RedfishError UpdateServiceSimpleUpdate(ctx).SimpleUpdateRequestBody(simpleUpdateRequestBody).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    simpleUpdateRequestBody := *openapiclient.NewSimpleUpdateRequestBody("ImageURI_example") // SimpleUpdateRequestBody | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.UpdateServiceSimpleUpdate(context.Background()).SimpleUpdateRequestBody(simpleUpdateRequestBody).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.UpdateServiceSimpleUpdate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateServiceSimpleUpdate`: RedfishError
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.UpdateServiceSimpleUpdate`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUpdateServiceSimpleUpdateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simpleUpdateRequestBody** | [**SimpleUpdateRequestBody**](SimpleUpdateRequestBody.md) |  | 

### Return type

[**RedfishError**](RedfishError.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

