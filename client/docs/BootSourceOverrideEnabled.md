# BootSourceOverrideEnabled

## Enum


* `ONCE` (value: `"Once"`)

* `CONTINUOUS` (value: `"Continuous"`)

* `DISABLED` (value: `"Disabled"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


