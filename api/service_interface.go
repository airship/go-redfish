package client

import (
	"context"
	"net/http"

	client "opendev.org/airship/go-redfish/client"
)

//go:generate mockery --name=RedfishAPI --output ./mocks
type RedfishAPI interface {
	CreateVirtualDisk(context.Context,
		string,
		string,
	) client.ApiCreateVirtualDiskRequest

	CreateVirtualDiskExecute(client.ApiCreateVirtualDiskRequest,
	) (client.RedfishError,
		*http.Response,
		error,
	)

	DeleteVirtualdisk(context.Context,
		string,
		string,
	) client.ApiDeleteVirtualdiskRequest

	DeleteVirtualdiskExecute(client.ApiDeleteVirtualdiskRequest,
	) (*http.Response,
		error,
	)

	EjectVirtualMedia(context.Context,
		string,
		string,
	) client.ApiEjectVirtualMediaRequest

	EjectVirtualMediaExecute(client.ApiEjectVirtualMediaRequest,
	) (client.RedfishError,
		*http.Response,
		error,
	)

	FirmwareInventory(context.Context,
	) client.ApiFirmwareInventoryRequest

	FirmwareInventoryDownloadImage(context.Context,
	) client.ApiFirmwareInventoryDownloadImageRequest

	FirmwareInventoryDownloadImageExecute(client.ApiFirmwareInventoryDownloadImageRequest,
	) (client.RedfishError,
		*http.Response,
		error,
	)

	FirmwareInventoryExecute(client.ApiFirmwareInventoryRequest,
	) (client.Collection,
		*http.Response,
		error,
	)

	GetManager(context.Context,
		string,
	) client.ApiGetManagerRequest

	GetManagerExecute(client.ApiGetManagerRequest,
	) (client.Manager,
		*http.Response,
		error,
	)

	GetManagerVirtualMedia(context.Context,
		string,
		string,
	) client.ApiGetManagerVirtualMediaRequest

	GetManagerVirtualMediaExecute(client.ApiGetManagerVirtualMediaRequest,
	) (client.VirtualMedia,
		*http.Response,
		error,
	)

	GetRoot(context.Context,
	) client.ApiGetRootRequest

	GetRootExecute(client.ApiGetRootRequest,
	) (client.Root,
		*http.Response,
		error,
	)

	GetSoftwareInventory(context.Context,
		string,
	) client.ApiGetSoftwareInventoryRequest

	GetSoftwareInventoryExecute(client.ApiGetSoftwareInventoryRequest,
	) (client.SoftwareInventory,
		*http.Response,
		error,
	)

	GetSystem(context.Context,
		string,
	) client.ApiGetSystemRequest

	GetSystemExecute(client.ApiGetSystemRequest,
	) (client.ComputerSystem,
		*http.Response,
		error,
	)

	GetTask(context.Context,
		string,
	) client.ApiGetTaskRequest

	GetTaskExecute(client.ApiGetTaskRequest,
	) (client.Task,
		*http.Response,
		error,
	)

	GetTaskList(context.Context,
	) client.ApiGetTaskListRequest

	GetTaskListExecute(client.ApiGetTaskListRequest,
	) (client.Collection,
		*http.Response,
		error,
	)

	GetVolumes(context.Context,
		string,
		string,
	) client.ApiGetVolumesRequest

	GetVolumesExecute(client.ApiGetVolumesRequest,
	) (client.Collection,
		*http.Response,
		error,
	)

	InsertVirtualMedia(context.Context,
		string,
		string,
	) client.ApiInsertVirtualMediaRequest

	InsertVirtualMediaExecute(client.ApiInsertVirtualMediaRequest,
	) (client.RedfishError,
		*http.Response,
		error,
	)

	ListManagerVirtualMedia(context.Context,
		string,
	) client.ApiListManagerVirtualMediaRequest

	ListManagerVirtualMediaExecute(client.ApiListManagerVirtualMediaRequest,
	) (client.Collection,
		*http.Response,
		error,
	)

	ListManagers(context.Context,
	) client.ApiListManagersRequest

	ListManagersExecute(client.ApiListManagersRequest,
	) (client.Collection,
		*http.Response,
		error,
	)

	ListSystems(context.Context,
	) client.ApiListSystemsRequest

	ListSystemsExecute(client.ApiListSystemsRequest,
	) (client.Collection,
		*http.Response,
		error,
	)

	ResetIdrac(context.Context,
	) client.ApiResetIdracRequest

	ResetIdracExecute(client.ApiResetIdracRequest,
	) (*http.Response,
		error,
	)

	ResetSystem(context.Context,
		string,
	) client.ApiResetSystemRequest

	ResetSystemExecute(client.ApiResetSystemRequest,
	) (client.RedfishError,
		*http.Response,
		error,
	)

	SetSystem(context.Context,
		string,
	) client.ApiSetSystemRequest

	SetSystemExecute(client.ApiSetSystemRequest,
	) (client.ComputerSystem,
		*http.Response,
		error,
	)

	UpdateService(context.Context,
	) client.ApiUpdateServiceRequest

	UpdateServiceExecute(client.ApiUpdateServiceRequest,
	) (client.UpdateService,
		*http.Response,
		error,
	)

	UpdateServiceSimpleUpdate(context.Context,
	) client.ApiUpdateServiceSimpleUpdateRequest

	UpdateServiceSimpleUpdateExecute(client.ApiUpdateServiceSimpleUpdateRequest,
	) (client.RedfishError,
		*http.Response,
		error,
	)
}
